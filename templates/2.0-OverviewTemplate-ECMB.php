<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/dist/images/temp/hero/hero-2.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->


<div class="body">

	<?php include('inc/i-rates.php'); ?>

	<section>
	
		<div class="sw pl">
			<div class="body-head">
				
				<div class="hgroup">
					<h1 class="hgroup-title">Why Us?</h1>
					<span class="hgroup-subtitle">We make sure your first big step is on solid ground.</span>
				</div><!-- .hgroup -->
				
				<p class="excerpt">
					Sed auctor aliquam accumsan. Duis ultricies molestie nisi, ac dictum arcu sagittis non. 
					In hendrerit, libero luctus tempor tempor, quam sem tempus tellus, in vehicula neque nisi et lectus.
				</p>
				
			</div><!-- .body-head -->

		</div><!-- .sw.pl -->
		
		<div class="sw pl">
			<div class="grid eqh ov-grid">
			
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<span class="circle-button primary big fa-building">The Company</span>
						
						<h4 class="hgroup-title">The Company</h4>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. 
							Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
							himenaeos. Duis sit amet diam turpis. 
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<span class="circle-button primary big fa-users">Meet our Team</span>
						
						<h4 class="hgroup-title">Meet Our Team</h4>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. 
							Class aptent taciti sociosqu ad litora torquent per.
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<span class="circle-button primary big fa-question-circle">Why A Mortgage Broker?</span>
						
						<h4 class="hgroup-title">Why A Mortgage Broker?</h4>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. 
							Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<span class="circle-button primary big fa-thumbs-up">Experience and Service</span>
						
						<h4 class="hgroup-title">Experience and Service</h4>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. 
							Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<span class="circle-button primary big fa-heart">In the Community</span>
						
						<h4 class="hgroup-title">In The Community</h4>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. 
							Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<span class="circle-button primary big fa-dollar">Best Rates</span>
						
						<h4 class="hgroup-title">Best Rates</h4>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. 
							Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<span class="circle-button primary big fa-newspaper-o">Subscribe to Newsletter</span>
						
						<h4 class="hgroup-title">Subscribe to Newsletter</h4>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. 
							Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos 
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				
			</div><!-- .grid -->
		</div><!-- .sw.pl -->	

	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>