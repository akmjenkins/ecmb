//rather than using Modernizr, which is big and bulky and unnecessary seeing how we only support modern browsers now anyway
;(function(context) {

	var $html = $('html');

	var tests = {
	
		touch: function() {
			return (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
		},
	
		ios: function() {
			return window.navigator.userAgent.match(/iphone|ipad/i);
		}	
	};

	//add these to the HTML tag, like modernizr would
	$.each(tests,function(name,cb) {
		$html.addClass(cb() ? name : 'no-'+name);
	});
	
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = tests;
	} else if(context) {
		context.tests = tests;
	}

}(typeof ns !== 'undefined' ? window[ns] : undefined));