<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/dist/images/temp/hero/hero-3.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->


<div class="body">

	<?php include('inc/i-rates.php'); ?>

	<section>
	
		<div class="sw pl">
			<div class="body-head">
				
				<div class="hgroup">
					<h1 class="hgroup-title">The Latest</h1>
					<span class="hgroup-subtitle">We make sure your first step is on solid groumd.</span>
				</div><!-- .hgroup -->
				
				<p class="excerpt">
					Sed auctor aliquam accumsan. Duis ultricies molestie nisi, ac dictum arcu sagittis non. 
					In hendrerit, libero luctus tempor tempor, quam sem tempus tellus, in vehicula neque nisi et lectus.
				</p>
				
			</div><!-- .body-head -->

		</div><!-- .sw.pl -->

	</section>
	
	<section class="dark-bg">
		<div class="sw pl">
		
			<h3>Latest News</h3>
			
			<div class="ov-grid grid eqh">
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
					
						<div class="circle-wrap sm bounce">
							<div class="circle lazybg img" data-src="../assets/dist/images/temp/resource-1.jpg">&nbsp;</div>
						</div><!-- .img -->
						
						<div class="hgroup">
							<h4 class="hgroup-title">News Post Title</h4>
							<span class="hgroup-subtitle">News Post Subtitle</span>
						</div><!-- .hgroup -->
						
						<time>Jan 15, 2015</time>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu 
							ad litora torquent per conubia nostra, per inceptos himenaeos. Duis sit amet diam turpis. 
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
					
						<div class="circle-wrap sm bounce">
							<div class="circle lazybg img" data-src="../assets/dist/images/temp/resource-1.jpg">&nbsp;</div>
						</div><!-- .img -->
						
						<div class="hgroup">
							<h4 class="hgroup-title">News Post Title</h4>
							<span class="hgroup-subtitle">News Post Subtitle</span>
						</div><!-- .hgroup -->
						
						<time>Jan 15, 2015</time>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu.
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
					
						<div class="circle-wrap sm bounce">
							<div class="circle lazybg img" data-src="../assets/dist/images/temp/resource-1.jpg">&nbsp;</div>
						</div><!-- .img -->
						
						<div class="hgroup">
							<h4 class="hgroup-title">News Post Title</h4>
							<span class="hgroup-subtitle">News Post Subtitle</span>
						</div><!-- .hgroup -->
						
						<time>Jan 15, 2015</time>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu. Lorem ipsum et dolor.
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
					
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .ov-grid -->
		
		</div><!-- .sw.pl -->
	</section><!-- .dark-bg -->
	
	<section>
		<div class="sw pl">
			
			<div class="grid ov-grid eqh">
				<div class="col col-2 xs-col-1">
					<div class="item ov-grid-item alleft">
					
						<h3>Latest Promotion</h3>
						
						<div class="promo-img lazybg" data-src="../assets/dist/images/temp/promotion-1.jpg">
						</div><!-- .with-img -->
						
						<div class="hgroup">
							<h4 class="hgroup-title">Promotion Title</h4>
							<span class="hgroup-subtitle">Promotion Subtitle</span>
						</div><!-- .hgroup -->
						
						<time>Ends January 30, 2015</time>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu 
							ad litora torquent per conubia nostra, per inceptos himenaeos. Duis sit amet diam turpis. 
						</p>
						
						<div class="btnwrap">
							<a class="button" href="#">Read More</a>
						</div><!-- .btnwrap -->
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2 xs-col-1">
					<div class="item ov-grid-item alleft">
					
						<h3>Latest Resource</h3>
						
						<div class="promo-img lazybg" data-src="../assets/dist/images/temp/promotion-1.jpg">
						</div><!-- .with-img -->
						
						<div class="hgroup">
							<h4 class="hgroup-title">Resource Title</h4>
							<span class="hgroup-subtitle">ResourceSubtitle</span>
						</div><!-- .hgroup -->
						
						<time>January 30, 2015</time>
						
						<p>
							Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu 
						</p>
						
						<div class="btnwrap">
							<a class="button" href="#">Read More</a>
						</div><!-- .btnwrap -->
					
					</div><!-- .item -->
				</div><!-- .col -->

			</div><!-- .grid -->
			
			
		</div><!-- .sw.pl -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>