<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav">
		
	<nav>
		<div class="sw">
			<ul>
				<li><a href="#">Why Us</a></li>
				<li><a href="#">First Time Home Buyers</a></li>
				<li><a href="#">Tools</a></li>
				<li><a href="#">Apply Now</a></li>
				<li><a href="#" class="t-fa-abs fa-navicon toggle-nav-overlay">More</a></li>
			</ul>			
		</div><!-- .sw -->
	</nav>
	
	<div class="nav-overlay d-bg">
		<div class="sw full">
		
			<span class="toggle-nav-overlay t-fa fa-close">Close</span>
			
			<div class="nav-overlay-grid">
				<div class="nav-overlay-col">
					<div class="nav-overlay-item">
						
						<h3>Apply for a Mortgage Now</h3>
						
						<p>
							Focused on ensuring your first home purchase is as pleasant as possible, our team has the knowledge and connections to guide you through the process.
						</p>
						
						<a href="#" class="button big">Apply Now</a>
						
					</div><!-- .nav-overlay-item -->
				</div><!-- .nav-overlay-col -->
				
				<div class="nav-overlay-col">
					<div class="nav-overlay-item">
						
						<div class="menu-item-group">
						
							<div class="menu-items">
								<h4>Other Services</h4>
								
								<ul>
									<li><a href="#">Renewals</a></li>
									<li><a href="#">Debt Management</a></li>
									<li><a href="#">Special Financing</a></li>
								</ul>								
							</div><!-- .menu-items -->
							
							<div class="menu-items">
								<h4>The Latest</h4>
								
								<ul>
									<li><a href="#">News</a></li>
									<li><a href="#">Promotions</a></li>
									<li><a href="#">Resources</a></li>
								</ul>
								
							</div><!-- .menu-items -->
							
						</div><!-- .menu-item-group -->
						
						<div class="menu-item-group">
							
							<div class="menu-items">
								<h4>Other Services</h4>
								
								<div class="nav-overlay-callouts">
								
									<span class="nav-overlay-callout callus">
										<span class="circle-button sm primary fa-phone">Call Us</span>
										<span class="t">709 754 0422</span>
									</span>
									
									<a class="nav-overlay-callout livechat">
										<span class="circle-button sm primary fa-comments">Live Chat</span>
										<span class="t">Live Chat</span>
									</a>
								
								</div><!-- .nav-overlay-callouts -->
								
							</div><!-- .menu-items -->
							
							<div class="menu-items">
								<h4>Social</h4>
								<?php include('i-social.php'); ?>
							</div><!-- .menu-items -->
							
						</div><!-- .menu-item-group -->
						
					</div><!-- .nav-overlay-item -->
				</div><!-- .nav-overlay-col -->
				
			</div><!-- .nav-overlay-grid -->
		
		</div><!-- .sw -->
	</div><!-- .nav-overlay -->
	
</div><!-- .nav -->