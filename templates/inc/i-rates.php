<div class="sw rates-wrap">

	<div class="rates">
	
		<div class="rates-bg">
	
			<div class="rates-contact">
				<span class="circle-button primary sm fa-phone">Call Now</span>
				<div>
					<strong class="block">Call Now!</strong> 709 754 0422
				</div>
			</div><!-- .rates-contact -->
			
			<span class="title">Today's Rates</span>
			
			<div class="rate-box">
				<small>Variable</small>
				<span class="rate">2.20%</span>
				<small>5 years closed variable</small>
			</div><!-- .rate-box -->
			
			<div class="rate-box">
				<small>Three Year</small>
				<span class="rate">2.49%</span>
				<small>3 years closed fixed</small>
			</div><!-- .rate-box -->
			
			<div class="rate-box">
				<small>Five Year</small>
				<span class="rate">2.79%</span>
				<small>5 years closed fixed</small>
			</div><!-- .rate-box -->
			
			<a href="#" class="button block">Apply Now</a>
		
		</div><!-- .rates-bg -->
		
		<div class="share">
			<span>Share</span>
			<a href="#" class="t-fa-abs fb">Share on Facebook</a>
			<a href="#" class="t-fa-abs tw">Share on Twitter</a>
		</div><!-- .share -->
	
	</div><!-- .rates -->
	
</div><!-- .sw.rates-wrap -->