<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/dist/images/temp/hero/hero-3.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->


<div class="body">

	<?php include('inc/i-rates.php'); ?>

	<section>
	
		<article>
	
			<div class="sw pl">
				<div class="body-head">
					
					<div class="hgroup">
						<h1 class="hgroup-title">News Single Post</h1>
						<span class="hgroup-subtitle">News Single Subtitle</span>
					</div><!-- .hgroup -->
					
					<time datetime="2015-01-15" pubdate>Jan 15, 2015</time>
					
					<p class="excerpt">
						Sed auctor aliquam accumsan. Duis ultricies molestie nisi, ac dictum arcu sagittis non. 
						In hendrerit, libero luctus tempor tempor, quam sem tempus tellus, in vehicula neque nisi et lectus.
					</p>
					
				</div><!-- .body-head -->
				
				<div class="article-body">
				
					<p>
						East Coast Mortgage Brokers is Newfoundland and Labrador’s premier mortgage company. Our goal is to treat our clients as our friends, 
						and to provide the best service, advice and rates possible. We ensure our clients receive the right mortgage solution suited to their 
						particular home financing need.
					</p>
						 
					<p>
						We are a member of the Verico Mortgage network, which gives us access to the majority of mortgage lenders in Canada. Through our 
						affiliation with Verico, we are able to access the very best rates, tools and services the industry has to offer, which allows us to provide 
						the most effective solutions to our customers.
					</p>
					
					<p>
						We have a well trained and experienced team of Mortgage advisors, many of whom have achieved the designation of Accredited Mortgage 
						Professional. This is the highest designation the industry has to offer, and it is designated through the Canadian Association of Accredited 
						Mortgage professionals, CAAMP. As a team of experienced professionals, we strictly adhere to the standards set by CAAMP.
					</p>
						 
					<p>
						Locally, we are a member of the St. John’s Board of Trade. We are also actively involved in the community, and a supporters of the Rotary Club of 
						St John’s Northwest, The Janeway Children’s Hospital,  Daffodil Place, The Seniors Resource Centre and other community organizations.
					</p>
				
				</div><!-- .article-body -->

			</div><!-- .sw.pl -->
		
		</article>

	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>