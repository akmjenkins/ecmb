			<footer class="dark-bg">
				<div class="sw pl">
					
					<div class="talk-mod">
						<div class="tm-item">
							<span class="title">Talk to us Today!</span>	
						</div>
						<div class="tm-item">
							<span class="t-fa fa-phone">709 754 0422</span>
						</div>
						<a href="#" class="tm-item">
							<span class="t-fa fa-comments">Live Chat</span>
						</a>
						<a href="#" class="tm-item">
							<span class="t-fa fa-envelope">Live Chat</span>
						</a>
					</div><!-- .talk-mod -->
					
					<div class="footer-nav">
						<ul>
							<li><a href="#">Why Us?</a></li>
							<li><a href="#">First Time Home Buyer</a></li>
							<li><a href="#">Renewals</a></li>
							<li><a href="#">Debt Management</a></li>
							<li><a href="#">Special Financing</a></li>
						</ul>
					</div><!-- .footer-nav -->
					
					<?php include('i-social.php'); ?>
					
					<div class="footer-contact">
					
						<address>
							<span>75 Tiffany Lane</span>
							<span>St. John's, NL</span>
							<span>A1A 4H7</span>
						</address>
					
					</div><!-- .footer-contact -->						
					
				</div><!-- .sw.pl -->
				
				<div class="copyright">
					<div class="sw pl">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">ECMB</a> All Rights Reserved.</li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
				
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/ecmb',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/dist/js/main.js"></script>
	</body>
</html>