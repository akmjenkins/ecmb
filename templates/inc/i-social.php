<div class="social">
	<a href="#" title="Like East Coast Mortgage Brokers on Facebook" class="social-fb" rel="external">Like East Coast Mortgage Brokers on Facebook</a>
	<a href="#" title="Follow East Coast Mortgage Brokers on Twitter" class="social-tw" rel="external">Follow East Coast Mortgage Brokers on Twitter</a>
	<a href="#" title="Get the latest from East Coast Mortgage Brokers via RSS" class="social-rss" rel="external">Get the latest from East Coast Mortgage Brokers via RSS</a>
</div><!-- .social -->