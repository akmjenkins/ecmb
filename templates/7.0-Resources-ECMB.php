<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/dist/images/temp/hero/hero-2.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->


<div class="body">

	<?php include('inc/i-rates.php'); ?>

	<section>
	
		<div class="sw pl">
			<div class="body-head">
				
				<div class="hgroup">
					<h1 class="hgroup-title">Resouces</h1>
					<span class="hgroup-subtitle">We make sure your first step is on solid groumd.</span>
				</div><!-- .hgroup -->
				
				<p class="excerpt">
					Sed auctor aliquam accumsan. Duis ultricies molestie nisi, ac dictum arcu sagittis non. 
					In hendrerit, libero luctus tempor tempor, quam sem tempus tellus, in vehicula neque nisi et lectus.
				</p>
				
			</div><!-- .body-head -->

		</div><!-- .sw.pl -->

	</section>
	
	<section class="filter-section">
	
		<div class="filter-bar">
			<div class="sw pl">
			
				<div class="filter-bar-content">
			
					<div class="filter-bar-left">
					
						<div class="count">
							<span class="num">20</span> Resources Found
						</div><!-- .tcount -->
						
					</div><!-- .filter-bar-left -->

					<div class="filter-bar-meta">
						
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
					</div><!-- .filter-bar-meta -->
				
				</div><!-- .filter-bar-content -->
			
			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		
		<div class="filter-content">
			<div class="sw pl">
			
				<div class="ov-grid grid eqh">
					<div class="col col-3 sm-col-2 xs-col-1">
						<div class="item ov-grid-item">
						
							<div class="circle-wrap sm bounce">
								<div class="circle lazybg img" data-src="../assets/dist/images/temp/resource-1.jpg">&nbsp;</div>
							</div><!-- .img -->
							
							<div class="hgroup">
								<h4 class="hgroup-title">Resource Title</h4>
								<span class="hgroup-subtitle">Resource Subtitle</span>
							</div><!-- .hgroup -->
							
							<time>Jan 15, 2015</time>
							
							<p>
								Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu 
								ad litora torquent per conubia nostra, per inceptos himenaeos. Duis sit amet diam turpis. 
							</p>
							
							<div class="btnwrap">
								<a href="#" class="button">Read More</a>
							</div><!-- .btnwrap -->
						
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<div class="item ov-grid-item">
						
							<div class="circle-wrap sm bounce">
								<div class="circle lazybg img" data-src="../assets/dist/images/temp/resource-1.jpg">&nbsp;</div>
							</div><!-- .img -->
							
							<div class="hgroup">
								<h4 class="hgroup-title">Resource Title</h4>
								<span class="hgroup-subtitle">Resource Subtitle</span>
							</div><!-- .hgroup -->
							
							<time>Jan 15, 2015</time>
							
							<p>
								Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu.
							</p>
							
							<div class="btnwrap">
								<a href="#" class="button">Read More</a>
							</div><!-- .btnwrap -->
						
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<div class="item ov-grid-item">
						
							<div class="circle-wrap sm bounce">
								<div class="circle lazybg img" data-src="../assets/dist/images/temp/resource-1.jpg">&nbsp;</div>
							</div><!-- .img -->
							
							<div class="hgroup">
								<h4 class="hgroup-title">Resource Title</h4>
								<span class="hgroup-subtitle">Resource Subtitle</span>
							</div><!-- .hgroup -->
							
							<time>Jan 15, 2015</time>
							
							<p>
								Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu. Lorem ipsum et dolor.
							</p>
							
							<div class="btnwrap">
								<a href="#" class="button">Read More</a>
							</div><!-- .btnwrap -->
						
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<div class="item ov-grid-item">
						
							<div class="circle-wrap sm bounce">
								<div class="circle lazybg img" data-src="../assets/dist/images/temp/resource-1.jpg">&nbsp;</div>
							</div><!-- .img -->
							
							<div class="hgroup">
								<h4 class="hgroup-title">Resource Title</h4>
								<span class="hgroup-subtitle">Resource Subtitle</span>
							</div><!-- .hgroup -->
							
							<time>Jan 15, 2015</time>
							
							<p>
								Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu 
								ad litora torquent per conubia nostra, per inceptos himenaeos. Duis sit amet diam turpis. 
							</p>
							
							<div class="btnwrap">
								<a href="#" class="button">Read More</a>
							</div><!-- .btnwrap -->
						
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<div class="item ov-grid-item">
						
							<div class="circle-wrap sm bounce">
								<div class="circle lazybg img" data-src="../assets/dist/images/temp/resource-1.jpg">&nbsp;</div>
							</div><!-- .img -->
							
							<div class="hgroup">
								<h4 class="hgroup-title">Resource Title</h4>
								<span class="hgroup-subtitle">Resource Subtitle</span>
							</div><!-- .hgroup -->
							
							<time>Jan 15, 2015</time>
							
							<p>
								Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu.
							</p>
							
							<div class="btnwrap">
								<a href="#" class="button">Read More</a>
							</div><!-- .btnwrap -->
						
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<div class="item ov-grid-item">
						
							<div class="circle-wrap sm bounce">
								<div class="circle lazybg img" data-src="../assets/dist/images/temp/resource-1.jpg">&nbsp;</div>
							</div><!-- .img -->
							
							<div class="hgroup">
								<h4 class="hgroup-title">Resource Title</h4>
								<span class="hgroup-subtitle">Resource Subtitle</span>
							</div><!-- .hgroup -->
							
							<time>Jan 15, 2015</time>
							
							<p>
								Donec vehicula ullamcorper purus, et maximus risus tempus sodales. Class aptent taciti sociosqu. Lorem ipsum et dolor.
							</p>
							
							<div class="btnwrap">
								<a href="#" class="button">Read More</a>
							</div><!-- .btnwrap -->
						
						</div><!-- .item -->
					</div><!-- .col -->					
				</div><!-- .ov-grid -->

			
			</div><!-- .sw.pl -->
		</div><!-- .filter-content -->
		
	</section><!-- .filter-section -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>