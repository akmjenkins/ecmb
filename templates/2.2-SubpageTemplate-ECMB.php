<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/dist/images/temp/hero/hero-3.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->


<div class="body">

	<?php include('inc/i-rates.php'); ?>

	<section>
	
		<article>
	
			<div class="sw pl">
				<div class="body-head">
					
					<div class="hgroup">
						<h1 class="hgroup-title">The Company</h1>
						<span class="hgroup-subtitle">We make sure your first big step is on solid ground.</span>
					</div><!-- .hgroup -->
					
					<p class="excerpt">
						Sed auctor aliquam accumsan. Duis ultricies molestie nisi, ac dictum arcu sagittis non. 
						In hendrerit, libero luctus tempor tempor, quam sem tempus tellus, in vehicula neque nisi et lectus.
					</p>
					
				</div><!-- .body-head -->
				
				<div class="article-body">
				
					<p>
						East Coast Mortgage Brokers is Newfoundland and Labrador’s premier mortgage company. Our goal is to treat our clients as our friends, 
						and to provide the best service, advice and rates possible. We ensure our clients receive the right mortgage solution suited to their 
						particular home financing need.
					</p>
						 
					<p>
						We are a member of the Verico Mortgage network, which gives us access to the majority of mortgage lenders in Canada. Through our 
						affiliation with Verico, we are able to access the very best rates, tools and services the industry has to offer, which allows us to provide 
						the most effective solutions to our customers.
					</p>
					
					<p>
						We have a well trained and experienced team of Mortgage advisors, many of whom have achieved the designation of Accredited Mortgage 
						Professional. This is the highest designation the industry has to offer, and it is designated through the Canadian Association of Accredited 
						Mortgage professionals, CAAMP. As a team of experienced professionals, we strictly adhere to the standards set by CAAMP.
					</p>
						 
					<p>
						Locally, we are a member of the St. John’s Board of Trade. We are also actively involved in the community, and a supporters of the Rotary Club of St John’s Northwest, The Janeway Children’s Hospital,  Daffodil Place, The Seniors Resource Centre and other community organizations.
					</p>
				
						<h2>Header 2 - H2</h2>
						<h3>Header 3 - H3</h3>
						<h4>Header 4 - H4</h4>
						<h5>Header 5 - H5</h5>
						<h6>Header 6 - H6</h6>

						<p>
							Nullam cursus, dui eget imperdiet dapibus, leo dui pretium libero, non facilisis massa felis et lacus. Suspendisse rutrum euismod turpis 
							vitae commodo. Sed in ante vel felis rutrum iaculis eget vitae ipsum. Praesent sollicitudin eros eu orci elementum porttitor. Aliquam efficitur 
							imperdiet volutpat. Pellentesque eget vestibulum dolor. Nunc sit amet pulvinar justo.
						</p>
						
						<h3>Links</h3>
						<p>
						
							<em>
								Note: styles apply to links in paragraphs/lists/blockquotes, or <code>&lt;a&gt;</code> tags with a class of <code>.inline</code>
							</em>
							
							<br />
							<br />
						
							<a href="#link">Link</a>
							<a href="#link" class="hover">Link:hover</a>
							<a href="#link" class="visited">Link:visited</a>
							<a href="#link" class="active">Link:active</a>
							
							<br />
							<br />
							
							And this is what a link will <a href="#link">look like</a> when it is written in a <a href="#link">regular paragraph</a>.
							
						</p>
						
						<hr />
						
						<ul>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>
								Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.
								<ul>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>								
								</ul>
							</li>
						</ul>
						
						<ol>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>
								Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.
								<ol>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>								
								</ol>
							</li>
						</ol>
				
				</div><!-- .article-body -->

			</div><!-- .sw.pl -->
		
		</article>

	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>