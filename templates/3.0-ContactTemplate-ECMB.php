<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/dist/images/temp/hero/hero-3.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->


<div class="body">

	<?php include('inc/i-rates.php'); ?>

	<section>
	
		<article>
	
			<div class="sw pl">
				<div class="body-head">
					
					<div class="hgroup">
						<h1 class="hgroup-title">Contact</h1>
						<span class="hgroup-subtitle">We make sure your first big step is on solid ground.</span>
					</div><!-- .hgroup -->
					
					<time datetime="2015-01-15" pubdate>Jan 15, 2015</time>
					
					<p class="excerpt">
						Sed auctor aliquam accumsan. Duis ultricies molestie nisi, ac dictum arcu sagittis non. 
						In hendrerit, libero luctus tempor tempor, quam sem tempus tellus, in vehicula neque nisi et lectus.
					</p>
					
				</div><!-- .body-head -->
				
				<div class="article-body">
				
					<p>
						Maecenas interdum tristique metus, vitae tincidunt augue cursus vel. Nullam id sem ut erat tincidunt vulputate id non nisl. Maecenas ac posuere mauris. 
						Morbi eu fermentum diam. Cras tincidunt urna a justo finibus, quis porttitor sem aliquet. Ut efficitur viverra venenatis.
					</p>
					
					<div class="contact-info-grid">
						<div class="contact-info">
							<div class="contact-info-block">
								<h3 class="title">Contact Info</h3>
								
								<address>
									75 Tiffany Lane <br />
									St. John's, NL
								</address>
								
								<br />
								
								<span>1 (709) 754-0422</span>
							</div><!-- .contact-info-block -->
							
							<div class="contact-info-block">
								<h4 class="title">Hours of Operation</h4>
								
								<div class="rows">
								
									<div class="row">
										<span class="l">Mon - Fri</span>
										<span class="r">9am - 5pm</span>
									</div><!-- .row -->
									
									<div class="row">
										<span class="l">Saturday</span>
										<span class="r">Closed</span>
									</div><!-- .row -->
									
									<div class="row">
										<span class="l">Sunday</span>
										<span class="r">Closed</span>
									</div><!-- .row -->
								
								</div><!-- .rows -->
								
							</div><!-- .contact-info-block -->
							
						</div><!-- .contact-info -->
						<div class="contact-info-map">
						
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2690.8742259872542!2d-52.7143701!3d47.5896884!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b0ca3f95847ba09%3A0xf4a7de82341651b6!2s75+Tiffany+Ln%2C+St.+John&#39;s%2C+NL+A1A+3B7!5e0!3m2!1sen!2sca!4v1425235617275" frameborder="0" style="border:0"></iframe>
							
						</div><!-- .contact-info-map -->
					</div><!-- .contact-info-grid -->
					
					<form action="/" class="body-form">
						<div class="fieldset">
							
							<div class="grid">
								<div class="col col-2 xs-col-1">
								<input type="text" name="fname" placeholder="First Name">
								</div><!-- .col -->
								<div class="col col-2 xs-col-1">
									<input type="text" name="lname" placeholder="Last Name">
								</div><!-- .col -->
								<div class="col col-1">
									<input type="email" name="email" placeholder="Email Address">
								</div>
								<div class="col col-1">
									<textarea name="message" placeholder="Message"></textarea>
								</div><!-- .col -->
							</div><!-- .grid -->
							
							<button class="button big">Submit</button>
							
						</div><!-- .fieldset -->
					</form><!-- .body-form -->
				
				</div><!-- .article-body -->

			</div><!-- .sw.pl -->
		
		</article>

	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>