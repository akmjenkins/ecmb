<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/dist/images/temp/hero/hero-3.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->


<div class="body">

	<?php include('inc/i-rates.php'); ?>

	<section>
	
		<div class="sw pl">
			<div class="body-head">
				
				<div class="hgroup">
					<h1 class="hgroup-title">Meet Our Team</h1>
					<span class="hgroup-subtitle">We make sure your first big step is on solid ground.</span>
				</div><!-- .hgroup -->
				
				<p class="excerpt">
					Sed auctor aliquam accumsan. Duis ultricies molestie nisi, ac dictum arcu sagittis non. 
					In hendrerit, libero luctus tempor tempor, quam sem tempus tellus, in vehicula neque nisi et lectus.
				</p>
				
			</div><!-- .body-head -->

		</div><!-- .sw.pl -->
		
		<div class="sw pl">
			<div class="grid eqh ov-grid">
			
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<div class="circle-wrap sm bounce">
							<div class="circle lazybg img" data-src="../assets/dist/images/temp/tm-1.jpg">&nbsp;</div>
						</div><!-- .img -->
						
						<div class="tm-title">
							<h4 class="hgroup-title name">Jill Wells</h4>
							<small class="title">Job Title</small>
						</div><!-- .tm-title -->
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<div class="circle-wrap sm bounce">
							<div class="circle lazybg img" data-src="../assets/dist/images/temp/tm-2.jpg">&nbsp;</div>
						</div><!-- .img -->
						
						<div class="tm-title">
							<h4 class="hgroup-title name">John Doe</h4>
							<small class="title">Job Title</small>
						</div><!-- .tm-title -->
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<div class="circle-wrap sm bounce">
							<div class="circle lazybg img" data-src="../assets/dist/images/temp/tm-3.jpg">&nbsp;</div>
						</div><!-- .img -->
						
						<div class="tm-title">
							<h4 class="hgroup-title name">Josh Smith</h4>
							<small class="title">Job Title</small>
						</div><!-- .tm-title -->
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<div class="circle-wrap sm bounce">
							<div class="circle lazybg img" data-src="../assets/dist/images/temp/tm-1.jpg">&nbsp;</div>
						</div><!-- .img -->
						
						<div class="tm-title">
							<h4 class="hgroup-title name">Jill Wells</h4>
							<small class="title">Job Title</small>
						</div><!-- .tm-title -->
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<div class="circle-wrap sm bounce">
							<div class="circle lazybg img" data-src="../assets/dist/images/temp/tm-2.jpg">&nbsp;</div>
						</div><!-- .img -->
						
						<div class="tm-title">
							<h4 class="hgroup-title name">John Doe</h4>
							<small class="title">Job Title</small>
						</div><!-- .tm-title -->
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<div class="circle-wrap sm bounce">
							<div class="circle lazybg img" data-src="../assets/dist/images/temp/tm-3.jpg">&nbsp;</div>
						</div><!-- .img -->
						
						<div class="tm-title">
							<h4 class="hgroup-title name">Josh Smith</h4>
							<small class="title">Job Title</small>
						</div><!-- .tm-title -->
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				
			</div><!-- .grid -->
		</div><!-- .sw.pl -->	

	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>