<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item with-content" data-src="
			../assets/dist/images/temp/hero/hero-1.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">
		
			<div class="hero-content">
				<div class="sw pl">
					
					<div class="hgroup">
						<h1 class="hgroup-title">First Time Home Buyer?</h1>
						<span class="hgroup-subtitle">We make sure your first big step is on solid ground.</span>
					</div><!-- .hgroup -->
					
					<p>
						Focused on ensuring your first home purchase is as pleasant as possible, our team has the knowledge and connections to provide you with the best rates available in the marketplace.
					</p>
					
					<a href="#" class="button big">Get Pre-Approved</a>
					
				</div><!-- .sw -->
			</div><!-- .hero-content -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->


<div class="body">

	<?php include('inc/i-rates.php'); ?>

	<section>
		<div class="sw pl">
			
			<div class="grid eqh ov-grid">
			
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<span class="circle-button primary big fa-users">Meet our Team</span>
						
						<h4 class="hgroup-title">Meet Our Team</h4>
						
						<p>
							Our experience, as both mortgage professionals and financial advisers, 
							ensures that you can trust our ability to provide sound advice in all aspects of home financing.
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<span class="circle-button primary big fa-calculator">Mortgage Calculators</span>
						
						<h4 class="hgroup-title">Mortgage Calculators</h4>
						
						<p>
							Calculate your mortgage payment. Discover how many years you will shorten your amortization 
							and how much interest savings you will realize by making a prepayment on your mortgage.
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item ov-grid-item">
						
						<span class="circle-button primary big fa-question-circle">Helpful Advice</span>
						
						<h4 class="hgroup-title">Helpful Advice</h4>	
						
						<p>
							Buying your first home can be a daunting experience, but at East Coast Mortgage Brokers, 
							our team is committed to making this experience as smooth and stress-free as possible.
						</p>
						
						<div class="btnwrap">
							<a href="#" class="button">Read More</a>
						</div><!-- .btnwrap -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid -->
			
		</div><!-- .sw -->	
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>