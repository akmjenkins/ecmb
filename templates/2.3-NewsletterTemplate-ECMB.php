<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/dist/images/temp/hero/hero-3.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->


<div class="body">

	<?php include('inc/i-rates.php'); ?>

	<section>
	
		<article>
	
			<div class="sw pl">
				<div class="body-head">
					
					<div class="hgroup">
						<h1 class="hgroup-title">Subscribe to Newsletter</h1>
						<span class="hgroup-subtitle">News Single Subtitle</span>
					</div><!-- .hgroup -->
					
					<time datetime="2015-01-15" pubdate>Jan 15, 2015</time>
					
					<p class="excerpt">
						Sed auctor aliquam accumsan. Duis ultricies molestie nisi, ac dictum arcu sagittis non. 
						In hendrerit, libero luctus tempor tempor, quam sem tempus tellus, in vehicula neque nisi et lectus.
					</p>
					
				</div><!-- .body-head -->
				
				<div class="article-body">
				
					<p>
						Maecenas interdum tristique metus, vitae tincidunt augue cursus vel. Nullam id sem ut erat tincidunt vulputate id non nisl. Maecenas ac posuere mauris. 
						Morbi eu fermentum diam. Cras tincidunt urna a justo finibus, quis porttitor sem aliquet. Ut efficitur viverra venenatis.
					</p>
					
					<form action="/" class="body-form">
						<div class="fieldset">
							
							<div class="grid">
								<div class="col col-2 xs-col-1">
								<input type="text" name="fname" placeholder="First Name">
								</div><!-- .col -->
								<div class="col col-2 xs-col-1">
									<input type="text" name="lname" placeholder="Last Name">
								</div><!-- .col -->
								<div class="col col-1">
									<input type="email" name="email" placeholder="Email Address">
								</div><!-- .col -->
							</div><!-- .grid -->
							
							<button class="button big">Submit</button>
							
						</div><!-- .fieldset -->
					</form><!-- .body-form -->

				</div><!-- .article-body -->
				

			</div><!-- .sw.pl -->
		
		</article>

	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>